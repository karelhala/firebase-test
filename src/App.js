import React, { Component } from 'react';
import './App.css';
import Index, { Auth } from './components/Layout';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import * as ROUTES from './constants/routes';

class App extends Component {
  render() {
    const firebase = false;
    return (
        <Router>
            <Switch>
                <Route path={ROUTES.LOG_IN} component={Auth} />
                <Route exact path="/" render={() => (
                    firebase ? (
                        <Redirect to="/dashboard" />
                    ) : (
                        <Redirect to="/login" />
                    )
                )} />
                {!firebase && <Route path="*" render={() => <Redirect to="/login" />} />}
                <Route exact path={ROUTES.LANDING} component={Index} />
                <Route path="*" render={() => <Redirect to="/" />} />
            </Switch>
        </Router>
    );
  }
}

export default App;
