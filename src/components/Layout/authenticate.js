import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import GoogleIcon from './google.svg';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SignIn from './sign-in';
import LogIn from './log-in';

const styles = theme => ({
    sameWidth: {
        flexGrow: 1
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 2
    },
});

class Authenticate extends React.Component {
    state = {
        value: 0
    }

    handleChange = (_event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <main className={classes.main}>
                <CssBaseline />
                <Paper className={classes.paper}>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChange}>
                            <Tab className={classes.sameWidth} label="Log In" />
                            <Tab className={classes.sameWidth} label="Sign In" />
                        </Tabs>
                    </AppBar>
                    {value === 0 && <LogIn />}
                    {value === 1 && <SignIn />}
                </Paper>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Sign in With
                </Typography>
                    <form className={classes.form}>
                        <Button
                            type="button"
                            variant="contained"
                            className={classes.submit}
                        >
                            <img src={GoogleIcon} alt="google icon" /> Google
                        </Button>
                        <Button
                            type="button"
                            variant="contained"
                            className={classes.submit}
                            disabled
                        >
                            Twitter
                        </Button>
                        <Button
                            type="button"
                            variant="contained"
                            className={classes.submit}
                            disabled
                        >
                            Github
                        </Button>
                    </form>
                </Paper>
            </main>
        )
    }
}

Authenticate.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Authenticate);
